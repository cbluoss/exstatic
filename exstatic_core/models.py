from decimal import Decimal
from statistics import median, mean, mode, stdev, quantiles, StatisticsError

from django.db import models as models
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import gettext as _
from django_extensions.db import fields as extension_fields


class University(models.Model):
    # Fields
    name = models.CharField(max_length=255)
    add_info = models.TextField(_('Weitere Informationen'), blank=True)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(_('Erstellt am'), auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(_('Zuletzt aktualisiert'), auto_now=True, editable=False)
    active = models.BooleanField(default=True)

    def head_departments(self):
        return self.departments.filter(head_department=None)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '%s' % self.name

    def get_absolute_url(self):
        return reverse('exstatic_core_university_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('exstatic_core_university_update', args=(self.slug,))


class ExamTopic(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    add_info = models.TextField(_('Weitere Informationen'), blank=True)
    created = models.DateTimeField(_('Erstellt am'), auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(_('Zuletzt aktualisiert'), auto_now=True, editable=False)
    active = models.BooleanField(default=True)

    # Relationship Fields
    department = models.ForeignKey(
        'exstatic_core.Department',
        on_delete=models.CASCADE, related_name="examtopics",
    )

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '%s/%s' % (self.name, self.department)

    def get_absolute_url(self):
        return reverse('exstatic_core_examtopic_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('exstatic_core_examtopic_update', args=(self.slug,))


class Department(models.Model):

    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    add_info = models.TextField(_('Weitere Informationen'), blank=True)
    created = models.DateTimeField(_('Erstellt am'), auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(_('Zuletzt aktualisiert'), auto_now=True, editable=False)
    active = models.BooleanField(default=True)

    # Relationship Fields
    university = models.ForeignKey(
        'exstatic_core.University',
        on_delete=models.CASCADE, related_name="departments",
    )
    head_department = models.ForeignKey(
        'exstatic_core.Department',
        on_delete=models.CASCADE, related_name="departments", blank=True, null=True
    )

    class Meta:
        ordering = ('university', 'name',)

    def __str__(self):
        return '%s' % self.name

    def get_absolute_url(self):
        return reverse('exstatic_core_department_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('exstatic_core_department_update', args=(self.slug,))


class Exam(models.Model):

    # Fields
    created = models.DateTimeField(_('Erstellt am'), auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(_('Zuletzt aktualisiert'), auto_now=True, editable=False)
    add_info = models.TextField(_('Weitere Informationen'), blank=True)
    date = models.DateTimeField()
    active = models.BooleanField(default=True)
    points_max = models.IntegerField()
    points_required = models.IntegerField()
    isGraded = models.BooleanField(default=False)

    # Relationship Fields
    topic = models.ForeignKey(
        'exstatic_core.ExamTopic',
        on_delete=models.CASCADE, related_name="exams",
    )

    clause = models.ForeignKey(
        'exstatic_core.EscalationClause',
        on_delete=models.SET_NULL, related_name="exams",
        blank=True, null=True
    )

    class Meta:
        ordering = ('-date',)

    def __str__(self):
        return '%s am %s' % (self.topic, self.date)

    def get_absolute_url(self):
        return reverse('exstatic_core_exam_detail', args=(self.pk,))

    def get_update_url(self):
        return reverse('exstatic_core_exam_update', args=(self.pk,))

    # statistic functions
    @cached_property
    def scores(self):
        """The exam scores are required a lot, we don't want to query them all the time."""
        return self.examscores.values_list('points', flat=True)

    def min(self):
        return min(self.scores)

    def max(self):
        return max(self.scores)

    def median(self):
        try:
            return median(self.scores)
        except StatisticsError:
            return "keine Daten"

    def mean(self):
        try:
            return round(mean(self.scores), 2)
        except StatisticsError:
            return "keine Daten"

    def mode(self):
        try:
            return mode(self.scores)
        except StatisticsError:
            return "keine Daten"

    def stdev(self):
        try:
            return round(stdev(self.scores), 2)
        except StatisticsError:
            return "keine Daten"

    def variance(self):
        try:
            return variance(self.scores)
        except StatisticsError:
            return "keine Daten"

    @cached_property
    def quartiles(self):
        try:
            return quantiles(self.scores)
        except StatisticsError:
            return "keine Daten"

    def percents_required(self):
        return round(self.real_points_required() / len(self.points_max) * 100)

    def real_points_required(self):
        "something fucked up, dunno what"
        if self.clause is None:
            return self.points_required
        if self.clause.students_min > self.examscores.all().count():
            return self.points_required
        points = self.examscores.values_list('points', flat=True)
        multi = Decimal(self.clause.percent_dev / 100.0)
        fc = min(self.points_required, Decimal((self.mean()) - (self.mean() * multi)))
        min_points = self.points_max * (self.clause.percent_min / 100.0)
        if min_points > fc:
            return round(min_points)
        return round(fc)

    def failed(self):
        return self.examscores.filter(points__lt=self.real_points_required()).count()

    def failed_percentage(self):
        return round(self.failed() / self.examscores.all().count() * 100)

    def api(self):
        return reverse('exam-detail', args=[self.pk])


class ExamScore(models.Model):

    # Fields
    points = models.DecimalField(max_digits=5, decimal_places=1)

    # Relationship Fields
    exam = models.ForeignKey(
        'exstatic_core.Exam',
        on_delete=models.CASCADE, related_name="examscores",
    )

    class Meta:
        ordering = ('-pk',)

    def __str__(self):
        return '%s' % self.points

    def get_absolute_url(self):
        return reverse('exstatic_core_examscore_detail', args=(self.pk,))


    def get_update_url(self):
        return reverse('exstatic_core_examscore_update', args=(self.pk,))


class EscalationClause(models.Model):
    """
    Some universities/exams have a escalation clause in place
    E.g: You've passed if your not n% below the average and got at least x%
    """
    # Fields
    name = models.CharField(max_length=255)
    slug = extension_fields.AutoSlugField(populate_from='name', blank=True)
    created = models.DateTimeField(_('Erstellt am'), auto_now_add=True, editable=False)
    last_updated = models.DateTimeField(_('Zuletzt aktualisiert'), auto_now=True, editable=False)
    percent_dev = models.IntegerField(default=30)
    percent_min = models.IntegerField(default=50, null=True, blank=True)
    students_min = models.PositiveIntegerField(default=30)


    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '%s' % self.slug

    def get_absolute_url(self):
        return reverse('exstatic_core_escalationclause_detail', args=(self.slug,))


    def get_update_url(self):
        return reverse('exstatic_core_escalationclause_update', args=(self.slug,))


