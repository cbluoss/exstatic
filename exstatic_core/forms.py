from django import forms
from .models import University, ExamTopic, Department, Exam, ExamScore, EscalationClause


class UniversityForm(forms.ModelForm):
    class Meta:
        model = University
        fields = ['name', 'add_info']


class ExamTopicForm(forms.ModelForm):
    class Meta:
        model = ExamTopic
        fields = ['name', 'add_info', 'department']


class DepartmentForm(forms.ModelForm):
    class Meta:
        model = Department
        fields = ['head_department', 'name', 'university', 'add_info',]


class ExamForm(forms.ModelForm):
    class Meta:
        model = Exam
        fields = ['topic', 'date', 'add_info', 'points_max', 'points_required', 'clause']

class ExamScoreForm(forms.ModelForm):
    class Meta:
        model = ExamScore
        fields = ['points', 'exam']


class EscalationClauseForm(forms.ModelForm):
    class Meta:
        model = EscalationClause
        fields = ['name', 'percent_dev', 'percent_min']


class ResultsForm(forms.Form):
    results = forms.CharField(widget=forms.Textarea, label='Ergebnisse')
    cutoff = forms.IntegerField(label="Unterer Grenzwert (um Noten rauszufiltern)", initial=4)