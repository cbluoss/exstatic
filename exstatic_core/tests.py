import unittest
import random
import string
from django.urls import reverse
from django.test import Client
from .models import University, ExamTopic, Department, Exam, ExamScore, EscalationClause
from django.contrib.auth.models import User
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType


def create_django_contrib_auth_models_user(**kwargs):
    defaults = {}
    defaults["username"] = "username"
    defaults["email"] = "username@tempurl.com"
    defaults.update(**kwargs)
    return User.objects.create(**defaults)


def create_django_contrib_auth_models_group(**kwargs):
    defaults = {}
    defaults["name"] = "group"
    defaults.update(**kwargs)
    return Group.objects.create(**defaults)


def create_django_contrib_contenttypes_models_contenttype(**kwargs):
    defaults = {}
    defaults.update(**kwargs)
    return ContentType.objects.create(**defaults)


def create_university(**kwargs):
    defaults = {}
    defaults["name"] = "name".join([random.choice(string.ascii_letters + string.digits) for n in range(4)])
    defaults["active"] = True
    defaults.update(**kwargs)
    return University.objects.create(**defaults)


def create_examtopic(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["active"] = True
    defaults.update(**kwargs)
    if "department" not in defaults:
        defaults["department"] = create_department()
    return ExamTopic.objects.create(**defaults)


def create_department(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["active"] = True
    defaults.update(**kwargs)
    if "university" not in defaults:
        defaults["university"] = create_university()
    # if "head_department" not in defaults:
    #     defaults["head_department"] = create_department()
    return Department.objects.create(**defaults)


def create_exam(**kwargs):
    defaults = {}
    defaults["date"] = "2019-04-16 12:00"
    defaults["active"] = True
    defaults["points_max"] = 50
    defaults["points_required"] = 30
    defaults.update(**kwargs)
    if "topic" not in defaults:
        defaults["topic"] = create_examtopic()
    return Exam.objects.create(**defaults)


def create_examscore(**kwargs):
    defaults = {}
    defaults["points"] = random.randint(0,50)
    defaults.update(**kwargs)
    if "exam" not in defaults:
        defaults["exam"] = create_exam()
    return ExamScore.objects.create(**defaults)


def create_escalationclause(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["percent_dev"] = "percent_dev"
    defaults["percent_min"] = "percent_min"
    defaults.update(**kwargs)
    return EscalationClause.objects.create(**defaults)


class UniversityViewTest(unittest.TestCase):
    '''
    Tests for University
    '''
    def setUp(self):
        self.client = Client()

    def test_list_university(self):
        url = reverse('exstatic_core_university_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_university(self):
        url = reverse('exstatic_core_university_create')
        data = {
            "name": "name",
            "active": "active",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_university(self):
        university = create_university()
        url = reverse('exstatic_core_university_detail', args=[university.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_university(self):
        university = create_university()
        data = {
            "name": "name_changed",
        }
        url = reverse('exstatic_core_university_update', args=[university.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ExamTopicViewTest(unittest.TestCase):
    '''
    Tests for ExamTopic
    '''
    def setUp(self):
        self.client = Client()

    def test_list_examtopic(self):
        url = reverse('exstatic_core_examtopic_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_examtopic(self):
        url = reverse('exstatic_core_examtopic_create')
        data = {
            "name": "name",
            "active": "active",
            "department": create_department().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_examtopic(self):
        examtopic = create_examtopic()
        url = reverse('exstatic_core_examtopic_detail', args=[examtopic.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_examtopic(self):
        examtopic = create_examtopic()
        data = {
            "name": "name",
            "active": "active",
            "department": create_department().pk,
        }
        url = reverse('exstatic_core_examtopic_update', args=[examtopic.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class DepartmentViewTest(unittest.TestCase):
    '''
    Tests for Department
    '''
    def setUp(self):
        self.client = Client()

    def test_list_department(self):
        url = reverse('exstatic_core_department_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_department(self):
        url = reverse('exstatic_core_department_create')
        data = {
            "name": "name",
            "active": "active",
            "university": create_university().pk,
            "head_department": create_department().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_department(self):
        department = create_department()
        url = reverse('exstatic_core_department_detail', args=[department.slug,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_department(self):
        department = create_department()
        data = {
            "name": "name",
            "active": "active",
            "university": create_university().pk,
            "head_department": create_department().pk,
        }
        url = reverse('exstatic_core_department_update', args=[department.slug,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class ExamViewTest(unittest.TestCase):
    '''
    Tests for Exam
    '''
    def setUp(self):
        self.client = Client()

    def test_list_exam(self):
        url = reverse('exstatic_core_exam_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_exam(self):
        url = reverse('exstatic_core_exam_create')
        data = {
            "date": "date",
            "active": "active",
            "points_max": "points_max",
            "points_required": "points_required",
            "topic": create_examtopic().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)

    def test_detail_exam(self):
        exam = create_exam()
        url = reverse('exstatic_core_exam_detail', args=[exam.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_exam(self):
        exam = create_exam()
        data = {
            "date": "date",
            "active": "active",
            "points_max": "points_max",
            "points_required": "points_required",
            "topic": create_examtopic().pk,
        }
        url = reverse('exstatic_core_exam_update', args=[exam.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 200)


# class ExamScoreViewTest(unittest.TestCase):
#     '''
#     Tests for ExamScore
#     '''
#     def setUp(self):
#         self.client = Client()

#     def test_list_examscore(self):
#         url = reverse('exstatic_core_examscore_list')
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)

#     def test_create_examscore(self):
#         url = reverse('exstatic_core_examscore_create')
#         data = {
#             "points": 15,
#             "exam": create_exam().pk,
#         }
#         response = self.client.post(url, data=data)
#         self.assertEqual(response.status_code, 200)

#     def test_detail_examscore(self):
#         examscore = create_examscore()
#         url = reverse('exstatic_core_examscore_detail', args=[examscore.pk,])
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)

#     def test_update_examscore(self):
#         examscore = create_examscore()
#         data = {
#             "points": 13,
#             "exam": create_exam().pk,
#         }
#         url = reverse('exstatic_core_examscore_update', args=[examscore.pk,])
#         response = self.client.post(url, data)
#         self.assertEqual(response.status_code, 200)


# class EscalationClauseViewTest(unittest.TestCase):
#     '''
#     Tests for EscalationClause
#     '''
#     def setUp(self):
#         self.client = Client()

#     def test_list_escalationclause(self):
#         url = reverse('exstatic_core_escalationclause_list')
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)

#     def test_create_escalationclause(self):
#         url = reverse('exstatic_core_escalationclause_create')
#         data = {
#             "name": "name",
#             "percent_dev": "percent_dev",
#             "percent_min": "percent_min",
#         }
#         response = self.client.post(url, data=data)
#         self.assertEqual(response.status_code, 302)

#     def test_detail_escalationclause(self):
#         escalationclause = create_escalationclause()
#         url = reverse('exstatic_core_escalationclause_detail', args=[escalationclause.slug,])
#         response = self.client.get(url)
#         self.assertEqual(response.status_code, 200)

#     def test_update_escalationclause(self):
#         escalationclause = create_escalationclause()
#         data = {
#             "name": "name",
#             "percent_dev": "percent_dev",
#             "percent_min": "percent_min",
#         }
#         url = reverse('exstatic_core_escalationclause_update', args=[escalationclause.slug,])
#         response = self.client.post(url, data)
#         self.assertEqual(response.status_code, 302)


