from django.contrib import admin
from django import forms
from .models import University, ExamTopic, Department, Exam, ExamScore, EscalationClause

class UniversityAdminForm(forms.ModelForm):

    class Meta:
        model = University
        fields = '__all__'


class UniversityAdmin(admin.ModelAdmin):
    form = UniversityAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'active']
    readonly_fields = ['created', 'last_updated']

admin.site.register(University, UniversityAdmin)


class ExamTopicAdminForm(forms.ModelForm):

    class Meta:
        model = ExamTopic
        fields = '__all__'


class ExamTopicAdmin(admin.ModelAdmin):
    form = ExamTopicAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'active']
    readonly_fields = ['created', 'last_updated',]

admin.site.register(ExamTopic, ExamTopicAdmin)


class DepartmentAdminForm(forms.ModelForm):

    class Meta:
        model = Department
        fields = '__all__'


class DepartmentAdmin(admin.ModelAdmin):
    form = DepartmentAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'active']
    readonly_fields = ['created', 'last_updated',]

admin.site.register(Department, DepartmentAdmin)


class ExamAdminForm(forms.ModelForm):

    class Meta:
        model = Exam
        fields = '__all__'


class ExamAdmin(admin.ModelAdmin):
    form = ExamAdminForm
    list_display = ['topic', 'date',  'points_max', 'points_required', 'clause', 'active', 'created', 'last_updated',]
    readonly_fields = ['created', 'last_updated']

admin.site.register(Exam, ExamAdmin)


class ExamScoreAdminForm(forms.ModelForm):

    class Meta:
        model = ExamScore
        fields = '__all__'


class ExamScoreAdmin(admin.ModelAdmin):
    form = ExamScoreAdminForm
    list_display = ['points']
    readonly_fields = ['points']

admin.site.register(ExamScore, ExamScoreAdmin)


class EscalationClauseAdminForm(forms.ModelForm):

    class Meta:
        model = EscalationClause
        fields = '__all__'


class EscalationClauseAdmin(admin.ModelAdmin):
    form = EscalationClauseAdminForm
    list_display = ['name', 'slug', 'created', 'last_updated', 'percent_dev', 'percent_min']
    readonly_fields = ['created', 'last_updated']

admin.site.register(EscalationClause, EscalationClauseAdmin)


