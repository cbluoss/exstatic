from . import models

from rest_framework import serializers


class UniversitySerializer(serializers.ModelSerializer):

    class Meta:
        model = models.University
        fields = (
            'slug',
            'name',
            'created',
            'last_updated',
            'active',
        )


class ExamTopicSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ExamTopic
        fields = (
            'slug',
            'name',
            'created',
            'last_updated',
            'active',
        )


class DepartmentSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Department
        fields = (
            'slug',
            'name',
            'created',
            'last_updated',
            'active',
        )


class ExamSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Exam
        fields = (
            'pk',
            'created',
            'last_updated',
            'date',
            'active',
            'points_max',
            'points_required',
            'clause',
            'examscores'
        )


class ExamScoreSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ExamScore
        fields = (
            'exam',
            'points',
        )


class EscalationClauseSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.EscalationClause
        fields = (
            'slug',
            'name',
            'created',
            'last_updated',
            'percent_dev',
            'percent_min',
        )


