from django.urls import path, include
from rest_framework import routers

from . import api
from . import views

router = routers.DefaultRouter()
router.register(r'university', api.UniversityViewSet)
router.register(r'examtopic', api.ExamTopicViewSet)
router.register(r'department', api.DepartmentViewSet)
router.register(r'exam', api.ExamViewSet)
router.register(r'examscore', api.ExamScoreViewSet)
router.register(r'escalationclause', api.EscalationClauseViewSet)


urlpatterns = (
    # urls for Django Rest Framework API
    path('api/v1/', include(router.urls)),
)

urlpatterns += (
    # urls for University
    path('university/', views.UniversityListView.as_view(), name='exstatic_core_university_list'),
    path('university/create/', views.UniversityCreateView.as_view(), name='exstatic_core_university_create'),
    path('university/detail/<slug:slug>/', views.UniversityDetailView.as_view(), name='exstatic_core_university_detail'),
    path('university/update/<slug:slug>/', views.UniversityUpdateView.as_view(), name='exstatic_core_university_update'),
)

urlpatterns += (
    # urls for ExamTopic
    path('examtopic/', views.ExamTopicListView.as_view(), name='exstatic_core_examtopic_list'),
    path('examtopic/create/', views.ExamTopicCreateView.as_view(), name='exstatic_core_examtopic_create'),
    path('examtopic/detail/<slug:slug>/', views.ExamTopicDetailView.as_view(), name='exstatic_core_examtopic_detail'),
    path('examtopic/update/<slug:slug>/', views.ExamTopicUpdateView.as_view(), name='exstatic_core_examtopic_update'),
)

urlpatterns += (
    # urls for Department
    path('department/', views.DepartmentListView.as_view(), name='exstatic_core_department_list'),
    path('department/create/', views.DepartmentCreateView.as_view(), name='exstatic_core_department_create'),
    path('department/detail/<slug:slug>/', views.DepartmentDetailView.as_view(), name='exstatic_core_department_detail'),
    path('department/update/<slug:slug>/', views.DepartmentUpdateView.as_view(), name='exstatic_core_department_update'),
)

urlpatterns += (
    # urls for Exam
    path('exam/', views.ExamListView.as_view(), name='exstatic_core_exam_list'),
    path('exam/create/', views.ExamCreateView.as_view(), name='exstatic_core_exam_create'),
    path('exam/detail/<int:pk>/', views.ExamDetailView.as_view(), name='exstatic_core_exam_detail'),
    path('exam/update/<int:pk>/', views.ExamUpdateView.as_view(), name='exstatic_core_exam_update'),
)

# urlpatterns += (
#     # urls for ExamScore
#     path('examscore/', views.ExamScoreListView.as_view(), name='exstatic_core_examscore_list'),
#     path('examscore/create/', views.ExamScoreCreateView.as_view(), name='exstatic_core_examscore_create'),
#     path('examscore/detail/<int:pk>/', views.ExamScoreDetailView.as_view(), name='exstatic_core_examscore_detail'),
#     path('examscore/update/<int:pk>/', views.ExamScoreUpdateView.as_view(), name='exstatic_core_examscore_update'),
# )

urlpatterns += (
    # urls for EscalationClause
    path('escalationclause/', views.EscalationClauseListView.as_view(), name='exstatic_core_escalationclause_list'),
    path('escalationclause/create/', views.EscalationClauseCreateView.as_view(), name='exstatic_core_escalationclause_create'),
    path('escalationclause/detail/<slug:slug>/', views.EscalationClauseDetailView.as_view(), name='exstatic_core_escalationclause_detail'),
    path('escalationclause/update/<slug:slug>/', views.EscalationClauseUpdateView.as_view(), name='exstatic_core_escalationclause_update'),
)

urlpatterns += (
    # Redirect empty paths to university overview
    path('', views.UniversityListView.as_view(), name='exstatic_core_university_list'),
)
