from . import models
from . import serializers
from rest_framework import viewsets, permissions


class UniversityViewSet(viewsets.ModelViewSet):
    """ViewSet for the University class"""

    queryset = models.University.objects.filter(active=True)
    serializer_class = serializers.UniversitySerializer
    permission_classes = [permissions.IsAuthenticated]


class ExamTopicViewSet(viewsets.ModelViewSet):
    """ViewSet for the ExamTopic class"""

    queryset = models.ExamTopic.objects.filter(active=True)
    serializer_class = serializers.ExamTopicSerializer
    permission_classes = [permissions.IsAuthenticated]


class DepartmentViewSet(viewsets.ModelViewSet):
    """ViewSet for the Department class"""

    queryset = models.Department.objects.filter(active=True)
    serializer_class = serializers.DepartmentSerializer
    permission_classes = [permissions.IsAuthenticated]


class ExamViewSet(viewsets.ModelViewSet):
    """ViewSet for the Exam class"""
    queryset = models.Exam.objects.filter(active=True)
    serializer_class = serializers.ExamSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        """
        Optionally restricts the returned exams to a given id
        """
        queryset = models.Exam.objects.filter(active=True)
        pk = self.request.query_params.get('id', None)
        if pk is not None:
            queryset = queryset.filter(pk=pk)
        return queryset


class ExamScoreViewSet(viewsets.ModelViewSet):
    """ViewSet for the ExamScore class"""

    queryset = models.ExamScore.objects.all()
    serializer_class = serializers.ExamScoreSerializer
    permission_classes = [permissions.IsAuthenticated]


class EscalationClauseViewSet(viewsets.ModelViewSet):
    """ViewSet for the EscalationClause class"""

    queryset = models.EscalationClause.objects.all()
    serializer_class = serializers.EscalationClauseSerializer
    permission_classes = [permissions.IsAuthenticated]


