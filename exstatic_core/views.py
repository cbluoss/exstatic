from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import DetailView, ListView, UpdateView, CreateView

from .forms import *


class UniversityListView(ListView):
    model = University


class UniversityCreateView(LoginRequiredMixin, CreateView):
    model = University
    form_class = UniversityForm


class UniversityDetailView(DetailView):
    model = University


class UniversityUpdateView(LoginRequiredMixin, UpdateView):
    model = University
    form_class = UniversityForm


class ExamTopicListView(ListView):
    model = ExamTopic


class ExamTopicCreateView(LoginRequiredMixin, CreateView):
    model = ExamTopic
    form_class = ExamTopicForm


class ExamTopicDetailView(DetailView):
    model = ExamTopic


class ExamTopicUpdateView(LoginRequiredMixin, UpdateView):
    model = ExamTopic
    form_class = ExamTopicForm


class DepartmentListView(ListView):
    model = Department
    queryset = Department.objects.filter(active=True).filter(head_department=None)


class DepartmentCreateView(LoginRequiredMixin, CreateView):
    model = Department
    form_class = DepartmentForm


class DepartmentDetailView(DetailView):
    model = Department


class DepartmentUpdateView(LoginRequiredMixin, UpdateView):
    model = Department
    form_class = DepartmentForm


class ExamListView(ListView):
    model = Exam


class ExamCreateView(LoginRequiredMixin, CreateView):
    model = Exam
    form_class = ExamForm

    def get_context_data(self, **kwargs):
        context = super(ExamCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['result_form'] = ResultsForm(self.request.POST)
            context['result_form'].full_clean()
        else:
            context['result_form'] = ResultsForm()
        return context

    def form_valid(self, form):
        context = self.get_context_data(form=form)
        resform = context['result_form']
        if resform.is_valid():
            response = super().form_valid(form)
            raw_results= resform.cleaned_data['results']
            # processing of result lists
            res = []
            for e in raw_results.split():
                try:
                    e = e.replace(",", ".")
                    if e.isalpha() or len(e) > 5: # skip everything longer than 5 chars (xx.x)
                        continue
                    e = float(e)
                    if e <= resform.cleaned_data['cutoff']:
                    # Skip extreme low values as they are probably grades.
                        continue
                    if e <= 60:
                        res.append(e)
                except:
                    pass
            for result in res:
                ExamScore.objects.create(points=result, exam=self.object)

            return response
        else:
            return super().form_invalid(form)

class ExamDetailView(DetailView):
    model = Exam


class ExamUpdateView(LoginRequiredMixin, UpdateView):
    model = Exam
    form_class = ExamForm


class ExamScoreListView(ListView):
    model = ExamScore


class ExamScoreCreateView(LoginRequiredMixin, CreateView):
    model = ExamScore
    form_class = ExamScoreForm


class ExamScoreDetailView(DetailView):
    model = ExamScore


class ExamScoreUpdateView(LoginRequiredMixin, UpdateView):
    model = ExamScore
    form_class = ExamScoreForm


class EscalationClauseListView(ListView):
    model = EscalationClause


class EscalationClauseCreateView(LoginRequiredMixin, CreateView):
    model = EscalationClause
    form_class = EscalationClauseForm


class EscalationClauseDetailView(DetailView):
    model = EscalationClause


class EscalationClauseUpdateView(LoginRequiredMixin, UpdateView):
    model = EscalationClause
    form_class = EscalationClauseForm
